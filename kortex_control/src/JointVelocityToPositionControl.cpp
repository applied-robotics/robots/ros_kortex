#include "kortex_control/JointVelocityToPositionControl.hpp"


JointVelocityToPositionControl::JointVelocityToPositionControl(ros::NodeHandle& nodeHandle) : node_handle_(nodeHandle) {

	// read parameters
	if ( !readParameters() ) {
		ROS_ERROR("[JointVelocityToPositionControl] Could not read parameters.");
		ros::requestShutdown();
	}
	ROS_DEBUG("[JointVelocityToPositionControl] Done reading parameters") ;


	/*==================== PUBLISHERS and SUBSCRIBERS =============================*/
/*
	des_joint_vel_topic_name_ 	= "/gen3/joint_group_velocity_controller/command" ;
	des_joint_pos_topic_name_ 	= "/gen3/joint_group_position_controller/command" ;
	fbk_joint_states_topic_name_= "/gen3/joint_states" ;
*/
	// subscribers
	des_joint_vel_subscriber_	= node_handle_.subscribe(des_joint_vel_topic_name_, 1, 		&JointVelocityToPositionControl::desJointVelocityCallback, 		this) ;
	fbk_state_subscriber_ 		= node_handle_.subscribe(fbk_joint_states_topic_name_, 1, 	&JointVelocityToPositionControl::fbkStateCallback, 		this) ;

	// publishers
	joint_position_publisher_ 	= node_handle_.advertise<std_msgs::Float64MultiArray>(des_joint_pos_topic_name_, 1) ;

	ready_ = false ;

	dt_ = 0.001 ;

	dim_joint_ = 7 ;
	fbk_joint_pos_ = Eigen::VectorXd::Zero(dim_joint_) ;
	des_joint_vel_ = Eigen::VectorXd::Zero(dim_joint_) ;
	des_joint_pos_ = Eigen::VectorXd::Zero(dim_joint_) ;

	for ( int i = 0 ; i < dim_joint_ ; i++ ) {
		joint_positions_.data.push_back(0.0) ;
	}
	ROS_INFO("Successfully launched JointVelocityToPositionControl");
}

JointVelocityToPositionControl::~JointVelocityToPositionControl() {}


bool JointVelocityToPositionControl::readParameters() {

	if ( !node_handle_.getParam("/model_name", model_name_) ) {
		ROS_ERROR("Cannot read /model_name");
		return false;
	}

	if ( !node_handle_.getParam("/dt", dt_ )) {
		ROS_ERROR ("Cannot read /dt, use default value" ) ;
		return false ;
	}

	// READ TOPIC NAMES
	if (!node_handle_.getParam("/topic_names/fbk_joint_state", fbk_joint_states_topic_name_ ) ) {
		ROS_ERROR("cannot read /topic_names/fbk_joint_state") ;
		return false;
	}

	if ( !node_handle_.getParam("/topic_names/cmd_joint_pos", des_joint_pos_topic_name_) ) {
		ROS_ERROR("Cannot read /topic_names/cmd_joint_pos");
		return false ;
	}

	if ( !node_handle_.getParam("/topic_names/cmd_joint_vel", des_joint_vel_topic_name_) ) {
		ROS_ERROR("Cannot read /topic_names/cmd_joint_vel");
		return false ;
	}

	return true;
}

void JointVelocityToPositionControl::update() {

	if ( !ready_ ) return ;

	ROS_DEBUG_STREAM("JointVelocityToPositionControl::update()");
	ROS_DEBUG_STREAM("des_joint_pos_= " << des_joint_pos_.transpose());
	des_joint_pos_ = fbk_joint_pos_ + des_joint_vel_ * dt_ ;

	joint_positions_.data[0] = des_joint_pos_(0) ;
	joint_positions_.data[1] = des_joint_pos_(1) ;
	joint_positions_.data[2] = des_joint_pos_(2) ;
	joint_positions_.data[3] = des_joint_pos_(3) ;
	joint_positions_.data[4] = des_joint_pos_(4) ;
	joint_positions_.data[5] = des_joint_pos_(5) ;
	joint_positions_.data[6] = des_joint_pos_(6) ;
	joint_position_publisher_.publish(joint_positions_) ;
}

void JointVelocityToPositionControl::fbkStateCallback(const sensor_msgs::JointState& message) {
	ready_ = true ;
	fbk_joint_pos_(0) = message.position[0] ;
	fbk_joint_pos_(1) = message.position[1] ;
	fbk_joint_pos_(2) = message.position[2] ;
	fbk_joint_pos_(3) = message.position[3] ;
	fbk_joint_pos_(4) = message.position[4] ;
	fbk_joint_pos_(5) = message.position[5] ;
	fbk_joint_pos_(6) = message.position[6] ;

	/*
	 * ROS_DEBUG_STREAM("[Potential Field] fbk_joint_pos_ = " << fbk_joint_pos_.transpose() ) ;
	fbk_joint_vel_(0) = message.velocity[0+joint_start_index] ;
	fbk_joint_vel_(1) = message.velocity[1+joint_start_index] ;
	fbk_joint_vel_(2) = message.velocity[2+joint_start_index] ;
	fbk_joint_vel_(3) = message.velocity[3+joint_start_index] ;
	fbk_joint_vel_(4) = message.velocity[4+joint_start_index] ;
	fbk_joint_vel_(5) = message.velocity[5+joint_start_index] ;
	fbk_joint_vel_(6) = message.velocity[6+joint_start_index] ;
	ROS_DEBUG_STREAM("[Potential Field] fbk_joint_vel_ = " << fbk_joint_vel_.transpose() ) ;
*/


}
void JointVelocityToPositionControl::desJointVelocityCallback(const std_msgs::Float64MultiArray& message) {
	des_joint_vel_(0) = message.data[0] ;
	des_joint_vel_(1) = message.data[1] ;
	des_joint_vel_(2) = message.data[2] ;
	des_joint_vel_(3) = message.data[3] ;
	des_joint_vel_(4) = message.data[4] ;
	des_joint_vel_(5) = message.data[5] ;
	des_joint_vel_(6) = message.data[6] ;
}


int main(int argc, char** argv) {
	ros::init(argc, argv, "initialize");
	ros::NodeHandle node_handle("~");

	ROS_INFO("initialize velocity controller");

	JointVelocityToPositionControl controller(node_handle) ;



	if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug)) {
		ros::console::notifyLoggerLevelsChanged();
	}

	ros::Rate looprate(1000) ;
	while (ros::ok() ) {
		ros::spinOnce() ;
		controller.update() ;
		looprate.sleep() ;
	}

	return true ;
}
