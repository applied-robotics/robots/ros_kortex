#include "kortex_control/JointVelocityControl.hpp"


JointVelocityControl::JointVelocityControl(ros::NodeHandle& nodeHandle) : node_handle_(nodeHandle) {

	// read parameters
//	if ( !readParameters() ) {
///		ROS_ERROR("[JointVelocityControl] Could not read parameters.");
		// ros::requestShutdown();
//	}
//	ROS_DEBUG("[JointVelocityControl] Done reading parameters") ;


	/*==================== PUBLISHERS and SUBSCRIBERS =============================*/

	des_joint_vel_topic_name_ 	= "/gen3/joint_velocity_controller/command" ;

	// subscribers
	des_joint_vel_subscriber_	= node_handle_.subscribe(des_joint_vel_topic_name_, 1, 	&JointVelocityControl::desJointVelocityCallback, 		this) ;

	// publishers
	joint_0_publisher_ 			= node_handle_.advertise<std_msgs::Float64>("/gen3/joint_velocity_controller_1/command", 1) ;
	joint_1_publisher_ 			= node_handle_.advertise<std_msgs::Float64>("/gen3/joint_velocity_controller_2/command", 1) ;
	joint_2_publisher_ 			= node_handle_.advertise<std_msgs::Float64>("/gen3/joint_velocity_controller_3/command", 1) ;
	joint_3_publisher_ 			= node_handle_.advertise<std_msgs::Float64>("/gen3/joint_velocity_controller_4/command", 1) ;
	joint_4_publisher_ 			= node_handle_.advertise<std_msgs::Float64>("/gen3/joint_velocity_controller_5/command", 1) ;
	joint_5_publisher_ 			= node_handle_.advertise<std_msgs::Float64>("/gen3/joint_velocity_controller_6/command", 1) ;
	joint_6_publisher_ 			= node_handle_.advertise<std_msgs::Float64>("/gen3/joint_velocity_controller_7/command", 1) ;

	ROS_INFO("Successfully launched potential field planner");
}

JointVelocityControl::~JointVelocityControl() {}


void JointVelocityControl::update() {

	joint_0_publisher_.publish(joint_vel_0_) ;
	joint_1_publisher_.publish(joint_vel_1_) ;
	joint_2_publisher_.publish(joint_vel_2_) ;
	joint_3_publisher_.publish(joint_vel_3_) ;
	joint_4_publisher_.publish(joint_vel_4_) ;
	joint_5_publisher_.publish(joint_vel_5_) ;
	joint_6_publisher_.publish(joint_vel_6_) ;
}


void JointVelocityControl::desJointVelocityCallback(const std_msgs::Float64MultiArray& message) {

	joint_vel_0_.data = message.data[0] ;
	joint_vel_1_.data = message.data[1] ;
	joint_vel_2_.data = message.data[2] ;
	joint_vel_3_.data = message.data[3] ;
	joint_vel_4_.data = message.data[4] ;
	joint_vel_5_.data = message.data[5] ;
	joint_vel_6_.data = message.data[6] ;

	update() ;
}

bool JointVelocityControl::readParameters() {

	if ( !node_handle_.getParam("/model_name", model_name_) ) {
		ROS_ERROR("Cannot read /model_name");
		return false;
	}

	if ( !node_handle_.getParam("/topic_names/des_joint_vel", des_joint_vel_topic_name_) ) {
		ROS_ERROR("Cannot read /topic_names/des_joint_vel");
		return false ;
	}

	return true;
}


int main(int argc, char** argv) {
	ros::init(argc, argv, "initialize");
	ros::NodeHandle node_handle("~");

	ROS_INFO("initialize velocity controller");

	JointVelocityControl controller(node_handle) ;

	ros::Rate looprate(1000) ;

	while (ros::ok() ) {
		ros::spinOnce() ;
		looprate.sleep() ;
	}

	return true ;
}
