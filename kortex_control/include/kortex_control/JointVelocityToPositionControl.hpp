#pragma once
#include <ros/ros.h>
#include <string>
#include <std_msgs/Float64MultiArray.h>
#include <sensor_msgs/JointState.h>
#include <Eigen/Dense>

class JointVelocityToPositionControl {

public:

	JointVelocityToPositionControl(ros::NodeHandle& nodeHandle);
	~JointVelocityToPositionControl();
	void update( ) ;

private:
	bool readParameters();
	void fbkStateCallback(const sensor_msgs::JointState& message) ;
	void desJointVelocityCallback(const std_msgs::Float64MultiArray& message) ;


	ros::NodeHandle& 	node_handle_ ; 				// ROS node handle.

	// publishers
	ros::Publisher joint_position_publisher_ ;

	// messages to be published
	std_msgs::Float64MultiArray joint_positions_ ;

	// subscribers
	ros::Subscriber 	des_joint_vel_subscriber_ ;		// Subscriber that subscribes to the feedback state
	ros::Subscriber 	fbk_state_subscriber_ ;

	Eigen::VectorXd fbk_joint_pos_ ;
	Eigen::VectorXd des_joint_vel_ ;
	Eigen::VectorXd des_joint_pos_ ;

	int dim_joint_ ;
	double dt_ ;
	bool ready_ ;
	std::string 	model_name_ ;					// ROS topic name for the robot model
	std::string 	des_joint_vel_topic_name_ ; 	// ROS topic name to publish to
	std::string 	des_joint_pos_topic_name_ ;
	std::string 	fbk_joint_states_topic_name_ ;
};
