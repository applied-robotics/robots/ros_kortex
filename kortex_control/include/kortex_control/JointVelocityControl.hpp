#pragma once
#include <ros/ros.h>
#include <string>
#include <std_msgs/Float64.h>
#include <std_msgs/Float64MultiArray.h>


class JointVelocityControl {

public:

	JointVelocityControl(ros::NodeHandle& nodeHandle);
	~JointVelocityControl();

private:

	bool readParameters();
	void desJointVelocityCallback(const std_msgs::Float64MultiArray& message) ;
	void update( ) ;

	ros::NodeHandle& 	node_handle_ ; 				// ROS node handle.

	// publishers
	ros::Publisher joint_0_publisher_ ;
	ros::Publisher joint_1_publisher_ ;
	ros::Publisher joint_2_publisher_ ;
	ros::Publisher joint_3_publisher_ ;
	ros::Publisher joint_4_publisher_ ;
	ros::Publisher joint_5_publisher_ ;
	ros::Publisher joint_6_publisher_ ;

	// messages to be published
	std_msgs::Float64 joint_vel_0_ ;
	std_msgs::Float64 joint_vel_1_ ;
	std_msgs::Float64 joint_vel_2_ ;
	std_msgs::Float64 joint_vel_3_ ;
	std_msgs::Float64 joint_vel_4_ ;
	std_msgs::Float64 joint_vel_5_ ;
	std_msgs::Float64 joint_vel_6_ ;

	// subscribers
	ros::Subscriber 	des_joint_vel_subscriber_ ;		// Subscriber that subscribes to the feedback state

	std::string 	model_name_ ;					// ROS topic name for the robot model
	std::string 	des_joint_vel_topic_name_ ; 	// ROS topic name to publish to

};
