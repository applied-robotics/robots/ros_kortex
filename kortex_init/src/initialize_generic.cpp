#include <ros/ros.h>
#include <controller_manager_msgs/LoadController.h>
#include <controller_manager_msgs/SwitchController.h>


int main(int argc, char** argv) {
	ros::init(argc, argv, "initialize");
	ros::NodeHandle node_handle("");

	ROS_INFO("initialize kortex");

	if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug)) {
		ros::console::notifyLoggerLevelsChanged();
	}


	/****************** LOAD CONTROLLER TYPE ***********************/
	std::string controller_type ;
	std::string controller_name ;
	if ( !node_handle.getParam("/controller/type", controller_type )) {
		ROS_ERROR ("Cannot read /controller/type" ) ;
		ros::requestShutdown();
	}
	else {
		ROS_DEBUG_STREAM ("/controller/type = " << controller_type ) ;
	}


	/****************** LOAD PARAMETERS *****************************/
	/*
	std::string file_name ;
	std::string load_command ;
	if ( controller_type.compare("kinematics") == 0 || controller_type.compare("kinematic") == 0) {
		if ( !node_handle.getParam("/gen3/kinematic_param", file_name )) {
			ROS_ERROR ("Cannot read /gen3/kinematic_param" ) ;
			ros::requestShutdown();
		}
		else {
			ROS_DEBUG_STREAM ("/gen3/kinematic_param = " << file_name ) ;
			load_command = "rosparam load " + file_name ;

			if ( system(load_command.c_str()) < 0 ) {
				ROS_ERROR("rosparam load failed") ;
				ros::requestShutdown();
			}
			else {
				ROS_DEBUG_STREAM("rosparam load done") ;
			}
		}
	}
	else if ( controller_type.compare("dynamics") == 0 || controller_type.compare("dynamic") == 0) {
		if ( !node_handle.getParam("/gen3/dynamic_param", file_name )) {
			ROS_ERROR ("Cannot read /gen3/dynamic_param" ) ;
			ros::requestShutdown();
		}
		else {
			ROS_DEBUG_STREAM ("/gen3/dynamic_param = " << file_name ) ;
			load_command = "rosparam load " + file_name ;

			if ( system(load_command.c_str()) < 0 ) {
				ROS_ERROR("rosparam load failed") ;
				ros::requestShutdown();
			}
			else {
				ROS_DEBUG_STREAM("rosparam load done") ;
			}
		}
	}
	else if ( controller_type.compare("velocity") == 0 || controller_type.compare("velocities") == 0) {
		controller_name = "joint_group_velocity_controller" ;
	}
	else {
		ROS_ERROR("Cannot find controller_type") ;
		ros::requestShutdown();
	}

*/

	/************************** DETERMINE CONTROLLER *************************/
	if ( !ros::service::waitForService("/gen3/controller_manager/load_controller", ros::Duration(30.0)) ) {
		ROS_ERROR_STREAM("Gazebo is not ready. Shutdown");
		return -1;
	}

	if ( controller_type.compare("kinematics") == 0 || controller_type.compare("kinematic") == 0) {
		controller_name = "joint_group_position_controller" ;
	}
	else if ( controller_type.compare("dynamics") == 0 || controller_type.compare("dynamic") == 0) {
		controller_name = "joint_group_effort_controller" ;
	}
	else if ( controller_type.compare("velocity") == 0 || controller_type.compare("velocities") == 0) {
		controller_name = "joint_group_velocity_controller" ;
	}
	else {
		ROS_ERROR("Cannot find controller_type") ;
		ros::requestShutdown();
	}

	ROS_DEBUG_STREAM("Wait for controller_manager_msgs::LoadController") ;

	ros::ServiceClient load_client = node_handle.serviceClient<controller_manager_msgs::LoadController>("/gen3/controller_manager/load_controller");
	controller_manager_msgs::LoadController load_msg ;

	load_msg.request.name = "joint_state_controller" ;
	if ( load_client.call(load_msg) ) {
		ROS_DEBUG_STREAM("joint_state_controller loaded  ");
	}
	else {
		ROS_ERROR_STREAM("cannot load joint_state_controller");
	}

	/* load kinematic controller or dynamic controller */
	load_msg.request.name = controller_name ;
	if ( load_client.call(load_msg) ) {
		ROS_DEBUG_STREAM(controller_name << " loaded ");
	}
	else {
		ROS_ERROR_STREAM("cannot load " << controller_name );
	}


	/**** switch controller ****/
	ROS_DEBUG_STREAM("Wait for controller_manager_msgs::SwitchController") ;
	if ( !ros::service::waitForService("/gen3/controller_manager/switch_controller", ros::Duration(20.0)) ) {
		ROS_ERROR_STREAM("Gazebo is not ready. Shutdown");
		return -1;
	}

	ros::ServiceClient switch_client = node_handle.serviceClient<controller_manager_msgs::SwitchController>("/gen3/controller_manager/switch_controller");
	controller_manager_msgs::SwitchController switch_msg ;


	bool with_gripper = false ;
	if ( !node_handle.getParam("/with_gripper", with_gripper )) {
		ROS_ERROR ("Cannot read /with_gripper" ) ;
	}

	if ( with_gripper ) {
		switch_msg.request.start_controllers = {"joint_state_controller", controller_name, "finger_group_action_controller" } ;
	}
	else {
		switch_msg.request.start_controllers = {"joint_state_controller", controller_name } ;
	}

	switch_msg.request.start_controllers = {"joint_state_controller", controller_name } ;
	switch_msg.request.strictness = 2 ;
	//switch_msg.request.timeout = 10 ;
	switch_client.call(switch_msg) ;
	ROS_DEBUG_STREAM("controller_manager_msgs::SwitchController");


	return 0;
}
