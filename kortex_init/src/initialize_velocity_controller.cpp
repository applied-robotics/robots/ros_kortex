#include <ros/ros.h>
#include <controller_manager_msgs/LoadController.h>
#include <controller_manager_msgs/SwitchController.h>


int main(int argc, char** argv) {
	ros::init(argc, argv, "initialize");
	ros::NodeHandle node_handle("~");

	ROS_INFO("initialize kortex");

	if (ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug)) {
		ros::console::notifyLoggerLevelsChanged();
	}

	std::string controller_type ;
	std::string controller_name ;
	if ( !node_handle.getParam("/controller_type", controller_type )) {
		ROS_ERROR ("Cannot read /controller_type" ) ;
		ros::requestShutdown();
	}
	else {
		ROS_DEBUG_STREAM ("controller_type = " << controller_type ) ;
	}

	ROS_DEBUG_STREAM("Wait for controller_manager_msgs::LoadController") ;


	if ( !ros::service::waitForService("/gen3/controller_manager/load_controller", ros::Duration(30.0)) ) {
		ROS_ERROR_STREAM("Gazebo is not ready. Shutdown");
		return -1;
	}


	ros::ServiceClient load_client = node_handle.serviceClient<controller_manager_msgs::LoadController>("/gen3/controller_manager/load_controller");
	controller_manager_msgs::LoadController load_msg ;

	load_msg.request.name = "joint_state_controller" ;
	if ( load_client.call(load_msg) ) {
		ROS_DEBUG_STREAM("controller_manager_msgs::LoadController");
	}
	else {
		ROS_ERROR_STREAM("cannot load joint_state_controller");
	}

	char digits[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
	controller_name = "joint_velocity_controller" ;

	for ( int i = 1 ; i < 8 ; i++ ) {
		/* load kinematic controller or dynamic controller */
		load_msg.request.name = controller_name + '_' + digits[i] ;

		ROS_DEBUG_STREAM("loading " << load_msg.request.name) ;
		if ( load_client.call(load_msg) ) {
			ROS_DEBUG_STREAM("controller_manager_msgs::LoadController");
		}
		else {
			ROS_ERROR_STREAM("cannot load " << controller_name );
		}
	}


	/**** switch controller ****/
	ROS_DEBUG_STREAM("Wait for controller_manager_msgs::SwitchController") ;
	if ( !ros::service::waitForService("/gen3/controller_manager/switch_controller", ros::Duration(20.0)) ) {
		ROS_ERROR_STREAM("Gazebo is not ready. Shutdown");
		return -1;
	}

	ros::ServiceClient switch_client = node_handle.serviceClient<controller_manager_msgs::SwitchController>("/gen3/controller_manager/switch_controller");
	controller_manager_msgs::SwitchController switch_msg ;

/*
	bool with_gripper = false ;
	if ( !node_handle.getParam("/with_gripper", with_gripper )) {
		ROS_ERROR ("Cannot read /with_gripper" ) ;
	}

	if ( with_gripper ) {
		switch_msg.request.start_controllers = {"joint_state_controller", controller_name, "finger_group_action_controller" } ;
	}
	else {
		switch_msg.request.start_controllers = {"joint_state_controller", controller_name } ;
	}
	*/


	switch_msg.request.start_controllers = {"joint_state_controller", "joint_velocity_controller_1", "joint_velocity_controller_2", "joint_velocity_controller_3", "joint_velocity_controller_4","joint_velocity_controller_5","joint_velocity_controller_6", "joint_velocity_controller_7"} ;
	switch_msg.request.strictness = 2 ;
	switch_client.call(switch_msg) ;
	ROS_DEBUG_STREAM("controller_manager_msgs::SwitchController");


	return 0;
}
